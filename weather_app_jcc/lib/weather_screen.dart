import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'config/model/one_call_response.dart';
import 'config/services/services.dart';
import 'package:weather_app_jcc/home_screen.dart';
import 'config/model/weather_response.dart';

class WeatherScreen extends StatefulWidget {
  WeatherScreen({Key? key}) : super(key: key);

  @override
  State<WeatherScreen> createState() => _WeatherScreenState();
}

class _WeatherScreenState extends State<WeatherScreen> {
  var data = Get.arguments;

  Services _services = Services();
  WeatherResponse? _weatherResponse;
  OneCallResponse? _oneCallResponse;
  String currentTemp = "";
  String greeting = "";

  @override
  void initState() {
    getData();
    getDay();
    greeting = "Selamat ${getTime()}, ${data[0]}!";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            constraints: const BoxConstraints.expand(),
            decoration: const BoxDecoration(
                gradient: LinearGradient(
              begin: Alignment(-1, -1),
              end: Alignment(1, 1),
              colors: [
                Color(0xff003f9a),
                Color(0xff003f9a),
                Color(0xff2871cc),
                Color(0xff468fea),
                Color(0xff5099f4)
              ],
            )),
            child: SafeArea(
              child: Padding(
                  padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                  child: Column(children: [
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(20)),
                          child: IconButton(
                            icon: FaIcon(
                              FontAwesomeIcons.chevronLeft,
                              color: Colors.black87,
                              size: 20,
                            ),
                            onPressed: () => Get.off(() => HomeScreen()),
                          ),
                        ),
                        Expanded(
                          child: Text(
                            "${data[1]}\n${getDay()}",
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white, fontSize: 18),
                          ),
                        ),
                        SizedBox(
                          width: 45,
                        )
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Expanded(
                      child: ListView(
                        physics: const ScrollPhysics(),
                        children: [
                          Text(
                            greeting,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                                fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          _weatherResponse != null
                              ? Column(
                                  children: [
                                    Image.network(_weatherResponse!.iconUrl),
                                    Text(
                                      '${_weatherResponse!.temp!.temperature}°ᶜ',
                                      style: TextStyle(
                                          fontSize: 40, color: Colors.white),
                                    ),
                                    Text(
                                      _weatherResponse!.weather!.description!,
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    SizedBox(
                                      height: 30,
                                    ),
                                    Card(
                                      elevation: 0,
                                      color: Colors.white10,
                                      child: Padding(
                                        padding: const EdgeInsets.all(16),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.stretch,
                                          children: <Widget>[
                                            Text(
                                              "Prakiraan Cuaca",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 16),
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            _oneCallResponse != null
                                                ? ListView.builder(
                                                    physics:
                                                        const NeverScrollableScrollPhysics(),
                                                    shrinkWrap: true,
                                                    itemCount: 5,
                                                    itemBuilder:
                                                        ((context, index) {
                                                      return ListTile(
                                                        leading: Image.network(
                                                          urlIcon(_oneCallResponse
                                                                  ?.daily?[
                                                                      index + 1]
                                                                  .weather?[0]
                                                                  .icon
                                                                  .toString() ??
                                                              '03d'),
                                                        ),
                                                        title: Text(getPlusDay(
                                                            index + 1)),
                                                        textColor: Colors.white,
                                                        subtitle: Text(
                                                            _oneCallResponse
                                                                    ?.daily?[
                                                                        index +
                                                                            1]
                                                                    .weather?[0]
                                                                    .description ??
                                                                'Tidak Diketahui'),
                                                        trailing: Text(
                                                          "Min: ${_oneCallResponse?.daily?[index + 1].temp?.min.toString()}°ᶜ\nMax: ${_oneCallResponse?.daily?[index + 1].temp?.max.toString()}°ᶜ",
                                                          textAlign:
                                                              TextAlign.justify,
                                                        ),
                                                      );
                                                    }))
                                                : Center(
                                                    child:
                                                        CircularProgressIndicator(),
                                                  )
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                )
                              : Column(children: [
                                  Text(
                                    "Mohon maaf, Data cuaca pada Kota yang Anda pilih belum tersedia.",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 20),
                                  ),
                                  SizedBox(
                                    height: 30,
                                  ),
                                  Container(
                                    width: 300,
                                    height: 40,
                                    child: TextButton(
                                      style: TextButton.styleFrom(
                                        backgroundColor: Colors.green,
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(20),
                                        ),
                                      ),
                                      onPressed: () {
                                        Get.off(() => HomeScreen());
                                      },
                                      child: Text(
                                        "Kembali ke Menu Utama",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w600),
                                      ),
                                    ),
                                  ),
                                ]),
                        ],
                      ),
                    )
                  ])),
            )));
  }

  void getData() async {
    final weatherResponse = await _services.getWeather(data[1]);
    setState(() => _weatherResponse = weatherResponse);
    final oneCallResponse = await _services.getOneCall(
        weatherResponse.coord!.lat.toString(),
        weatherResponse.coord!.lon.toString());
    setState(() => _oneCallResponse = oneCallResponse);
  }

  String getDay() {
    var day = DateTime.now();
    return DateFormat('EEEE, dd MMMM yyyy', 'id_ID').format(day);
  }

  String getPlusDay(plus) {
    var now = DateTime.now();
    var t = now.add(Duration(days: plus));
    return DateFormat('EEEE, dd MMMM yyyy', 'id_ID').format(t);
  }

  String getTime() {
    var hour = DateTime.now().hour;
    if (hour > 2 && hour < 10) {
      return 'Pagi';
    } else if (hour < 15) {
      return 'Siang';
    } else if (hour < 18) {
      return 'Sore';
    } else {
      return 'Malam';
    }
  }

  String urlIcon(icon) {
    return 'https://openweathermap.org/img/wn/$icon@2x.png';
  }
}
