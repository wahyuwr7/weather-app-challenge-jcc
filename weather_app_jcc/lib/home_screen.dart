import 'dart:convert';
import 'dart:io';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:weather_app_jcc/weather_screen.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final TextEditingController _nameController = TextEditingController();

  List<String>? provinces = [];

  List _cities = [];

  List<String>? cities = [];

  var parsedData;

  String? selectedCity;

  String selectedProvince = '';

  int provinceIndex = -1;

  Future<void> parseJson() async {
    final String response =
        await rootBundle.loadString('assets/city_list.json');
    parsedData = await json.decode(response);

    for (int i = 0; i < 34; i++) {
      provinces!.add(parsedData[i]['provinsi']);
    }
  }

  @override
  void initState() {
    super.initState();
    parseJson();
  }

  void setCities(index) {
    setState(() {
      getCities(index);
    });
  }

  getCities(index) {
    cities = [];
    _cities = [];
    _cities = parsedData[index]['kota'];
    for (int i = 0; i < _cities.length; i++) {
      cities!.add(_cities[i]);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          constraints: const BoxConstraints.expand(),
          decoration: const BoxDecoration(
              gradient: LinearGradient(
            begin: Alignment(-1, -1),
            end: Alignment(1, 1),
            colors: [
              Color(0xff003f9a),
              Color(0xff003f9a),
              Color(0xff2871cc),
              Color(0xff468fea),
              Color(0xff5099f4),
              Color(0xff78c1ff)
            ],
          )),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Aplikasi Cuaca",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 24,
                    fontWeight: FontWeight.w600),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: [
                      TextField(
                          controller: _nameController,
                          cursorColor: Colors.white,
                          style: TextStyle(color: Colors.white),
                          decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.white, width: 2.0),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.white54, width: 2.0),
                              ),
                              labelText: "Masukkan Nama Anda",
                              labelStyle: TextStyle(color: Colors.white70))),
                      SizedBox(
                        height: 20,
                      ),
                      DropdownSearch<String>(
                        mode: Mode.DIALOG,
                        showSelectedItems: false,
                        showSearchBox: true,
                        items: provinces,
                        dropdownSearchBaseStyle: TextStyle(color: Colors.white),
                        dropdownSearchDecoration: const InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.white, width: 2.0),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.white54, width: 2.0),
                            ),
                            labelText: "Pilih Provinsi",
                            labelStyle: TextStyle(color: Colors.white70)),
                        onChanged: (newValue) {
                          selectedProvince = newValue!;
                          getProvinceIndex(selectedProvince);
                          setCities(provinceIndex);
                        },
                        selectedItem: selectedProvince,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      DropdownSearch<String>(
                        mode: Mode.DIALOG,
                        showSelectedItems: true,
                        showSearchBox: true,
                        items: cities,
                        dropdownSearchDecoration: const InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.white, width: 2.0),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.white54, width: 2.0),
                            ),
                            labelText: "Pilih Kota",
                            labelStyle: TextStyle(color: Colors.white70)),
                        onChanged: (newValue) {
                          selectedCity = newValue!;
                        },
                        selectedItem: selectedCity,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                        width: double.infinity,
                        height: 45,
                        child: TextButton(
                          style: TextButton.styleFrom(
                            backgroundColor: Colors.deepOrangeAccent,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20),
                            ),
                          ),
                          onPressed: () {
                            buttonHandler();
                          },
                          child: Text(
                            "Lihat Cuaca",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.w600),
                          ),
                        ),
                      ),
                    ],
                  )),
            ],
          )),
    );
  }

  void getProvinceIndex(String selectedProvince) {
    for (int i = 0; i < 34; i++) {
      if (provinces![i] == selectedProvince) {
        provinceIndex = i;
        break;
      }
    }
  }

  void buttonHandler() async {
    try {
      final result = await InternetAddress.lookup('openweathermap.org');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        if (_nameController.text.isNotEmpty) {
          if (provinceIndex != -1) {
            if (selectedCity != null) {
              Get.off(() => WeatherScreen(), arguments: [
                _nameController.text.toString(),
                selectedCity.toString()
              ]);
            } else {
              ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                content: Text(
                  'Mohon Pilih Kota Terlebih Dahulu!',
                  style: TextStyle(color: Colors.white),
                ),
                backgroundColor: Colors.red,
              ));
            }
          } else {
            ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
              content: Text(
                'Mohon Pilih Provinsi Terlebih Dahulu!',
                style: TextStyle(color: Colors.white),
              ),
              backgroundColor: Colors.red,
            ));
          }
        } else {
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            content: Text(
              'Mohon Isikan Nama Anda!',
              style: TextStyle(color: Colors.white),
            ),
            backgroundColor: Colors.red,
          ));
        }
      }
    } on SocketException catch (_) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text(
          'Ups! Tidak Ada Koneksi Internet.',
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.red,
      ));
    }
  }
}
