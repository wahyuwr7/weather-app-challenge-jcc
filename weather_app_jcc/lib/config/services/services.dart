import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:weather_app_jcc/config/model/one_call_response.dart';
import 'package:weather_app_jcc/config/model/weather_response.dart';

const apiKey = '284ff7d3d7b1dcaf6c0b00f7642b3b20';

class Services {
  Future<WeatherResponse> getWeather(String city) async {
    final queryParams = {
      'q': city,
      "lang": "id",
      "units": "metric",
      "appId": apiKey
    };

    final uri =
        Uri.https("api.openweathermap.org", "/data/2.5/weather", queryParams);

    final response = await http.get(uri);

    final json = jsonDecode(response.body);

    return WeatherResponse.fromJson(json);
  }

  Future<OneCallResponse> getOneCall(String? lat, String? lon) async {
    final queryParams = {
      'lat': lat,
      'lon': lon,
      "lang": "id",
      'exclude': 'minutely,hourly,alerts',
      "units": "metric",
      "appId": apiKey
    };

    final uri =
        Uri.https("api.openweathermap.org", "/data/2.5/onecall", queryParams);

    final response = await http.get(uri);

    final json = jsonDecode(response.body);

    return OneCallResponse.fromJson(json);
  }
}
