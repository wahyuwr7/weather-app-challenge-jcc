class Weather {
  final String? description;
  final String? icon;

  Weather({this.description, this.icon});

  factory Weather.fromJson(Map<String, dynamic> json) {
    final description = json['description'];
    final icon = json['icon'];
    return Weather(description: description, icon: icon);
  }
}

class Coord {
  final double? lat;
  final double? lon;

  Coord({this.lat, this.lon});

  factory Coord.fromJson(Map<String, dynamic> json) {
    final lat = json['lat'];
    final lon = json['lon'];
    return Coord(lat: lat, lon: lon);
  }
}

class Temperature {
  final double? temperature;

  Temperature({this.temperature});

  factory Temperature.fromJson(Map<String, dynamic> json) {
    final temperature = json['temp'];
    return Temperature(temperature: temperature);
  }
}

class WeatherResponse {
  final String? cityName;
  final Temperature? temp;
  final Weather? weather;
  final Coord? coord;

  String get iconUrl {
    return 'https://openweathermap.org/img/wn/${weather!.icon}@2x.png';
  }

  WeatherResponse({this.cityName, this.temp, this.weather, this.coord});

  factory WeatherResponse.fromJson(Map<String, dynamic> json) {
    final cityName = json['name'];

    final tempJson = json['main'];
    final temp = Temperature.fromJson(tempJson);

    final weatherJson = json['weather'][0];
    final weather = Weather.fromJson(weatherJson);

    final coordJson = json['coord'];
    final coord = Coord.fromJson(coordJson);

    return WeatherResponse(
        cityName: cityName, temp: temp, weather: weather, coord: coord);
  }
}
